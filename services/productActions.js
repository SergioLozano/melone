import cuid from "cuid";
import * as ImageManipulator from "expo-image-manipulator";
import * as firebase from "firebase";
import "@firebase/firestore";

export async function saveNewProduct(images) {
  const user = firebase.auth().currentUser;
  const firestore = firebase.firestore();
  const folder = cuid();
  const newProductsRef = firestore.collection("NewProducts").doc(folder);
  const path = `users/${user.uid}/products_images/${folder}`;
  const ref = firebase.storage().ref(path);
  const date = new Date();

  newProductsRef.set({
    created_at: date,
    userId: user.uid,
    verified: false
  });
  try {
    await images.map(async image => {
      const resizeResult = await ImageManipulator.manipulateAsync(image.uri, [
        { compress: 1, resize: { width: 1024 } }
      ]);

      const compressedImage = resizeResult.uri;

      const imageName = image.name;
      const ext = compressedImage.split(".").pop();
      const response = await fetch(compressedImage);
      const blob = await response.blob();

      var uploadTask = ref.child(`${imageName}.${ext}`).put(blob);

      uploadTask.on(
        "state_changed",
        function(snapshot) {
          var progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        },
        function(error) {
          console.log(error);
          return Promise.resolve({ type: "error" });
        },
        function() {
          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            newProductsRef.update({ [imageName]: downloadURL });
          });
        }
      );
    });
    return Promise.resolve({ type: "success" });
  } catch (error) {
    return Promise.resolve({ type: "error" });
  }
}
