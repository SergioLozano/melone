import * as firebase from "firebase";

export async function getUserData() {
  const user = firebase.auth().currentUser;
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users").doc(user.uid);
  try {
    return userRef.get().then(function(doc) {
      if (doc.exists) {
        return doc;
      }
    });
  } catch (error) {
    return error;
  }
}

export async function savePhotoURL(file) {
  const user = firebase.auth().currentUser;
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users").doc(user.uid);
  const path = `users/${user.uid}/profile_images/`;
  const ref = firebase.storage().ref(path);
  const imageName = "profile";
  const ext = file.split(".").pop();
  const response = await fetch(file);
  const blob = await response.blob();

  var uploadTask = ref.child(`${imageName}.${ext}`).put(blob);

  uploadTask.on(
    "state_changed",
    function(snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log("Upload is " + progress + "% done");
    },
    function(error) {
      console.log(error);
    },
    function() {
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        userRef.update({ photoURL: downloadURL });
        user.updateProfile({ photoURL: downloadURL });
      });
    }
  );
}

export async function updateUserName(username) {
  const user = firebase.auth().currentUser;
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users").doc(user.uid);

  await userRef.update({ username });
  user.updateProfile({ displayName: username });

  console.log(user)
}
