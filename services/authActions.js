import * as firebase from "firebase";
import * as GoogleSignIn from "expo-google-sign-in";
import * as Facebook from "expo-facebook";
import { AsyncStorage } from "react-native";
import { AuthSession } from "expo";
import config from "../config";

const AUTH_BASE_URL = "https://melone.herokuapp.com";
const REDIRECT_URL = AuthSession.getRedirectUrl();

export async function signInWithFacebook() {
  const appId = Expo.Constants.manifest.extra.facebook.appId;
  const permissions = ["public_profile", "email"]; // Permissions required
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users");

  const { type, token } = await Facebook.logInWithReadPermissionsAsync(appId, {
    permissions
  });

  switch (type) {
    case "success": {
      await firebase
        .auth()
        .setPersistence(firebase.auth.Auth.Persistence.LOCAL); // Set persistent auth state
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      await firebase
        .auth()
        .signInWithCredential(credential)
        .then(function(result) {
          if (result.additionalUserInfo.isNewUser) {
            userRef.doc(result.user.uid).set({
              email: result.user.email,
              emailVerified: result.user.emailVerified,
              photoURL: result.user.photoURL,
              username: result.additionalUserInfo.profile.name,
              firstName: result.additionalUserInfo.profile.first_name,
              middleName: result.additionalUserInfo.profile.middle_name,
              lastName: result.additionalUserInfo.profile.last_name,
              providerId: result.additionalUserInfo.providerId,
              last_logged_in: Date.now(),
              createdAt: Date.now()
            });
          } else {
            userRef.doc(result.user.uid).update({
              last_logged_in: Date.now()
            });
          }
        })
        .catch(error => {
          return Promise.resolve({ type: "error", error });
        });

      // Do something with Facebook profile data
      // OR you have subscribed to auth state change, authStateChange handler will process the profile data

      return Promise.resolve({ type: "success" });
    }
    case "cancel": {
      return Promise.reject({ type: "cancel" });
    }
  }
}

async function getTwitterRequestToken() {
  const url = `${AUTH_BASE_URL}/auth/twitter/request_token`;
  const res = await fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      redirect_uri: REDIRECT_URL
    })
  });
  return await res.json();
}

async function getTwitterAccessToken(params) {
  const { oauth_token, oauth_token_secret, oauth_verifier } = params;
  const url = `${AUTH_BASE_URL}/auth/twitter/access_token`;
  const res = await fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ oauth_token, oauth_token_secret, oauth_verifier })
  });
  return await res.json();
}

export async function signInWithTwitter() {
  const { oauth_token, oauth_token_secret } = await getTwitterRequestToken();
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users");

  const { params } = await AuthSession.startAsync({
    authUrl: `https://api.twitter.com/oauth/authenticate?oauth_token=${oauth_token}`
  });

  const oauth_verifier = params.oauth_verifier;
  const result = await getTwitterAccessToken({
    oauth_token,
    oauth_token_secret,
    oauth_verifier
  });

  const credential = firebase.auth.TwitterAuthProvider.credential(
    result.oauth_token,
    result.oauth_token_secret
  );

  await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); // Set persistent auth state

  await firebase
    .auth()
    .signInWithCredential(credential)
    .then(function(result) {
      if (result.additionalUserInfo.isNewUser) {
        userRef.doc(result.user.uid).set({
          email: result.user.email,
          emailVerified: result.user.emailVerified,
          username: result.additionalUserInfo.username,
          photoURL: result.user.photoURL,
          name: result.additionalUserInfo.profile.name,
          description: result.additionalUserInfo.profile.description,
          providerId: result.additionalUserInfo.providerId,
          verified: result.additionalUserInfo.profile.verified,
          last_logged_in: Date.now(),
          createdAt: Date.now()
        });
        return Promise.resolve({ type: "success" });
      } else {
        userRef.doc(result.user.uid).update({
          last_logged_in: Date.now()
        });
        return Promise.resolve({ type: "success" });
      }
    })
    .catch(error => {
      return Promise.resolve({ type: "error" });
    });
}

export async function initGoogleSignInAsync() {
  try {
    await GoogleSignIn.initAsync({
      clientId: config.IOS_REVERSE_CLIENT_ID
    });
    this._syncUserWithStateAsync();
  } catch ({ message }) {
    alert("GoogleSignIn.initAsync(): " + message);
  }
}

_syncUserWithStateAsync = async () => {
  const user = await GoogleSignIn.signInSilentlyAsync();
  this.onSignIn(user);
};

export async function signInWithGoogle() {
  try {
    await GoogleSignIn.askForPlayServicesAsync();
    const { type, user } = await GoogleSignIn.signInAsync();
    if (type === "success") {
      this._syncUserWithStateAsync();
    }
  } catch ({ message }) {
    alert("login: Error:" + message);
    reject(message);
  }
}

export async function signOut(provider) {
  try {
    if (provider === "google.com") {
      await GoogleSignIn.signOutAsync();
    }
    firebase.auth().signOut();
    await AsyncStorage.clear();
  } catch ({ message }) {
    alert("Ha ocurrido un error inesperado...");
  }
}

onSignIn = async googleUser => {
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users");
  var credential = firebase.auth.GoogleAuthProvider.credential(
    googleUser.auth.idToken,
    googleUser.auth.accessToken
  );
  // Sign in with credential from the Google user.
  await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); // Set persistent auth state
  await firebase
    .auth()
    .signInWithCredential(credential)
    .then(function(result) {
      if (result.additionalUserInfo.isNewUser) {
        userRef
          .doc(result.user.uid)
          .set({
            email: result.user.email,
            emailVerified: result.user.emailVerified,
            username: result.additionalUserInfo.profile.name,
            photoURL: result.user.photoURL,
            name: result.additionalUserInfo.profile.given_name,
            familyName: result.additionalUserInfo.profile.family_name,
            providerId: result.additionalUserInfo.providerId,
            locale: result.additionalUserInfo.profile.locale,
            last_logged_in: Date.now(),
            createdAt: Date.now()
          })
          .then(function(snapshot) {
            // console.log('Snapshot', snapshot);
          })
          .catch(function(error) {
            return alert(error.message);
          });
      } else {
        userRef
          .doc(result.user.uid)
          .update({
            last_logged_in: Date.now()
          })
          .then(function(snapshot) {
            // console.log('Snapshot', snapshot);
          })
          .catch(function(error) {
            return alert(error.message);
          });
      }
    });
};

export async function signInWithEmail(values) {
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users");
  await firebase
    .auth()
    .signInWithEmailAndPassword(values.email, values.password)
    .then(function(result) {
      userRef.doc(result.user.uid).update({
        last_logged_in: Date.now()
      });
    })
    .catch(function(error) {
      if (error.code === "auth/user-not-found") {
        error.message = "Email o contraseña incorrectos.";
      } else if (error.code === "auth/wrong-password") {
        error.message = "Contraseña incorrecta.";
      } else {
        error.message =
          "En este momento nuestros sistemas están sobrecargados. Por favor, inténtalo de nuevo dentro de unos minutos.";
      }
      return Promise.reject(error);
    });
}

export async function signUpWithEmail(values) {
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users");

  let query = userRef.where("username", "==", values.username).limit(1);

  let querySnap = await query.get();

  if (querySnap.docs.length === 0) {
    await firebase
      .auth()
      .createUserWithEmailAndPassword(values.email, values.password)
      .then(function(result) {
        if (result.additionalUserInfo.isNewUser) {
          firebase
            .auth()
            .currentUser.updateProfile({ displayName: values.username });
          userRef
            .doc(result.user.uid)
            .set({
              email: result.user.email,
              emailVerified: result.user.emailVerified,
              photoURL: result.user.photoURL,
              username: values.username,
              providerId: result.additionalUserInfo.providerId,
              createdAt: Date.now()
            })
            .catch(function(error) {
              if (error.code === "auth/email-already-in-use") {
                error.message =
                  "El email introducido ya se encuentra registrado.";
              } else {
                error.message =
                  "En este momento nuestros sistemas están sobrecargados. Por favor, inténtalo de nuevo dentro de unos minutos.";
              }

              return Promise.reject(error);
            });
        }
      })
      .catch(function(error) {
        if (error.code === "auth/email-already-in-use") {
          error.message = "El email introducido ya se encuentra registrado.";
        } else {
          error.message =
            "En este momento nuestros sistemas están sobrecargados. Por favor, inténtalo de nuevo dentro de unos minutos.";
        }
        return Promise.reject(error);
      });
  } else {
    const error = {
      code: "auth/username-already-in-use",
      message: "Nombre de usuario no disponible."
    };

    return Promise.reject(error);
  }
}

export async function sendResetPasswordResetEmail(value) {
  await firebase
    .auth()
    .sendPasswordResetEmail(value.email)
    .then(function(result) {
      Promise.resolve(result);
    })
    .catch(function(error) {
      console.log(error);
      return Promise.reject(error);
    });
}
