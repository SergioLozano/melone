import React, { Component } from "react";
import { ScrollView, Dimensions } from "react-native";
import { withStyles, Text, Layout, Button } from "react-native-ui-kitten";
import { NutrimentCardList } from "./nutrimentsList/NutrimentsCardList";
import { textStyle } from "../common/style";
import { NutrientsChart } from "./nutrimentsList/NutrientsChart";
import { AdditivesList } from "./additiveList/AdditivesList";
import { IngredientsTags } from "./ingredients/IngredientsTags";
import { OverviewCard } from "./overview/OverviewCard";

import additivesList from "../../core/data/additives";

const deviceWidth = Dimensions.get("window").width;

class NutrimentInfoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nutrients: [
        {
          label: "Proteínas",
          value: 0,
          type: "proteins",
          color: "rgb(0, 206, 106)"
        },
        {
          label: "Hidratos de carbono",
          value: 0,
          type: "carbohydrates",
          color: "rgb(140, 83, 255)"
        },
        {
          label: "Grasas",
          value: 0,
          type: "fat",
          color: "rgb(64, 124, 255)"
        },
        {
          label: "Grasas saturadas",
          value: 0,
          type: "saturated_fat",
          color: "rgb(255, 183, 0)"
        },
        {
          label: "Sal",
          value: 0,
          type: "salt",
          color: "rgb(255, 183, 0)"
        },
        {
          label: "Fibra",
          value: 0,
          type: "fiber",
          color: "rgb(0, 224, 139)"
        },
        {
          label: "Azúcares",
          value: 0,
          type: "sugars",
          color: "rgb(233, 30, 99)"
        }
      ],
      nutrientsCards: [
        {
          label: "Calorias",
          value: 0,
          type: "calories",
          color: "rgb(64, 124, 255)"
        },
        {
          label: "Grasas saturadas",
          value: 0,
          type: "saturated_fat",
          color: "rgb(255, 183, 0)"
        },
        {
          label: "Sal",
          value: 0,
          type: "salt",
          color: "rgb(255, 183, 0)"
        },
        {
          label: "Azúcares",
          value: 0,
          type: "sugars",
          color: "rgb(233, 30, 99)"
        }
      ],
      ingredientsTags: [],
      additives: [],
      additivesCount: {
        state: false,
        low: 0,
        moderate: 0,
        high: 0
      },
      ingredientsCount: 0,
      process: {
        title: "Producto sin clasificar",
        subtitle: "",
        novaScore: 0,
        color: "#edf0f4"
      }
    };
  }

  nutrientsValues = data => {
    return [
      {
        label: "Proteínas",
        value: data.nutriments.proteins_100g
          ? data.nutriments.proteins_100g
          : 0,
        type: "proteins",
        color: "rgb(0, 206, 106)"
      },
      {
        label: "Hidratos de carbono",
        value: data.nutriments.carbohydrates_100g
          ? data.nutriments.carbohydrates_100g
          : 0,
        type: "carbohydrates",
        color: "rgb(140, 83, 255)"
      },
      {
        label: "Grasas",
        value: data.nutriments.fat_100g ? data.nutriments.fat_100g : 0,
        type: "fat",
        color: "rgb(64, 124, 255)"
      },
      {
        label: "Grasas saturadas",
        value: data.nutriments["saturated-fat_100g"]
          ? data.nutriments["saturated-fat_100g"]
          : 0,
        type: "saturated_fat",
        color: "rgb(255, 183, 0)"
      },
      {
        label: "Sal",
        value: data.nutriments.salt_100g ? data.nutriments.salt_100g : 0,
        type: "salt",
        color: "rgb(255, 183, 0)"
      },
      {
        label: "Fibra",
        value: data.nutriments.fiber_100g ? data.nutriments.fiber_100g : 0,
        type: "fiber",
        color: "rgb(0, 224, 139)"
      },
      {
        label: "Azúcares",
        value: data.nutriments.sugars_100g ? data.nutriments.sugars_100g : 0,
        type: "sugars",
        color: "rgb(233, 30, 99)"
      }
    ];
  };

  cardsValues = data => {
    return [
      {
        label: "Calorias",
        value: data.nutriments.energy_value ? data.nutriments.energy_value : 0,
        type: "calories",
        color: "rgb(64, 124, 255)"
      },
      {
        label: "Grasas saturadas",
        value: data.nutriments["saturated-fat_100g"]
          ? data.nutriments["saturated-fat_100g"]
          : 0,
        type: "saturated_fat",
        color: "rgb(255, 183, 0)"
      },
      {
        label: "Sal",
        value: data.nutriments.salt_100g ? data.nutriments.salt_100g : 0,
        type: "salt",
        color: "rgb(255, 183, 0)"
      },
      {
        label: "Azúcares",
        value: data.nutriments.sugars_100g ? data.nutriments.sugars_100g : 0,
        type: "sugars",
        color: "rgb(233, 30, 99)"
      }
    ];
  };

  getIngredientsTags = () => {
    const analysis = this.props.info.ingredients_analysis_tags;
    if (analysis) {
      const labels = this.props.info.labels
        ? this.props.info.labels.split(", ")
        : [];
      let ingredientsTags = [];

      if (analysis.includes("en:palm-oil")) {
        ingredientsTags.push({ label: "Aceite de palma", color: "#e91e63" });
      }

      if (analysis.includes("en:non-vegan")) {
        ingredientsTags.push({ label: "No vegano", color: "#e91e63" });
      }
      if (analysis.includes("en:palm-oil-free")) {
        ingredientsTags.push({
          label: "Sin aceite de palma",
          color: "#00ce6a"
        });
      }
      if (labels.includes("Vegetariano")) {
        ingredientsTags.push({ label: "Vegetariano", color: "#00ce6a" });
      }
      if (labels.includes("Vegano")) {
        ingredientsTags.push({ label: "Vegano", color: "#00ce6a" });
      }
      if (labels.includes("Sin gluten")) {
        ingredientsTags.push({ label: "Sin gluten", color: "#00ce6a" });
      }

      this.setState({ ingredientsTags });
    }
  };

  componentDidMount() {
    const { info } = this.props;

    const nutrients = this.nutrientsValues(info);
    const cardsInfo = this.cardsValues(info);
    const energyUnit = info.nutriments.energy_unit
      ? info.nutriments.energy_unit
      : "Kcal";

    if (nutrients && cardsInfo) {
      this.setState({ nutrients, nutrientsCards: cardsInfo, energyUnit });
    }

    this.getIngredientsTags();
    const additives = info.additives_tags;

    if (additives && additives.length !== 0) {
      let names = new Set(
        additives.map(additive => additive.replace("en:", ""))
      );

      let filteredAdditives = additivesList.filter(e => names.has(e.name));

      filteredAdditives.map(a => {
        if (a.risk === "High risks") {
          a.color = "#e91e63";
          a.sort = 3;
          this.setState(prevState => ({
            additivesCount: {
              state: true,
              low: prevState.additivesCount.low,
              moderate: prevState.additivesCount.moderate,
              high: prevState.additivesCount.high + 1
            }
          }));
        } else if (a.risk === "Moderate risks") {
          a.color = "#ffb700";
          a.sort = 2;
          this.setState(prevState => ({
            additivesCount: {
              state: true,
              low: prevState.additivesCount.low,
              moderate: prevState.additivesCount.moderate + 1,
              high: prevState.additivesCount.high
            }
          }));
        } else if (a.risk === "Low risks") {
          a.color = "#2AC260";
          a.sort = 1;
          this.setState(prevState => ({
            additivesCount: {
              state: true,
              low: prevState.additivesCount.low + 1,
              moderate: prevState.additivesCount.moderate,
              high: prevState.additivesCount.high
            }
          }));
        } else {
          a.color = "#edf0f4";
          a.sort = 0;
        }
        return a;
      });

      filteredAdditives.sort(function(a, b) {
        return a.sort - b.sort;
      });

      this.setState({ additives: filteredAdditives });
    }

    const ingredientsCount = info.ingredients_n;
    const novaScore = info.nova_groups;

    if (novaScore) {
      if (isNaN(novaScore)) {
        novaScore = Number(novaScore);
      }

      if (novaScore === 1) {
        this.setState({
          process: {
            title: "Minimamente procesado",
            subtitle: "Nada o mínimamente procesado",
            novaScore: novaScore,
            color: "#00ce6a"
          }
        });
      } else if (novaScore === 2) {
        this.setState({
          process: {
            title: " Ingrediente culinario",
            subtitle: "Ingrediente culinario procesado",
            novaScore: novaScore,
            color: "#ffb700"
          }
        });
      } else if (novaScore === 3) {
        this.setState({
          process: {
            title: "Alimento complementario",
            subtitle: "Buen procesado",
            novaScore: novaScore,
            color: "#ffb700"
          }
        });
      } else if (novaScore >= 4) {
        this.setState({
          process: {
            title: "Alimento a evitar",
            subtitle: "Ultraprocesado",
            novaScore: novaScore,
            color: "#e91e63"
          }
        });
      }
    } else if (ingredientsCount) {
      if (isNaN(ingredientsCount)) {
        ingredientsCount = Number(ingredientsCount);
      }
      if (ingredientsCount <= 1) {
        this.setState({
          process: {
            title: "Minimamente procesado",
            subtitle: "Nada o mínimamente procesado",
            novaScore: 1,
            color: "#00ce6a"
          }
        });
      } else if (ingredientsCount > 1 && ingredientsCount <= 5) {
        this.setState({
          process: {
            title: "Alimento complementario",
            subtitle: "Buen procesado",
            novaScore: 3,
            color: "#ffb700"
          }
        });
      } else if (ingredientsCount > 5) {
        this.setState({
          process: {
            title: "Alimento a evitar",
            subtitle: "Ultraprocesado",
            novaScore: 4,
            color: "#e91e63"
          }
        });
      }
    }

    if (ingredientsCount) {
      this.setState({ ingredientsCount });
    }
  }

  render() {
    const { themedStyle, info } = this.props;
    const {
      additives,
      nutrients,
      nutrientsCards,
      ingredientsTags,
      additivesCount,
      ingredientsCount,
      process,
      energyUnit
    } = this.state;

    const allergens = info.allergens_tags;
    let allergensCount;

    if (allergens) {
      allergensCount = Object.keys(allergens).length;
    } else {
      allergensCount = 0;
    }

    return (
      <ScrollView style={themedStyle.container}>
        <OverviewCard
          image={info.image_front_url}
          ingredientsTags={ingredientsTags}
          process={process}
          allergens={allergensCount}
          additives={additivesCount}
        />
        <NutrimentCardList
          contentContainerStyle={themedStyle.cardContainer}
          data={nutrientsCards}
          energyUnit={energyUnit}
        />
        <Text
          style={[textStyle.subtitle, themedStyle.categoryTitle]}
          category="h4"
        >
          {`Información nutricional`}
        </Text>
        <NutrientsChart values={nutrients} deviceWidth={deviceWidth} />

        <Layout style={themedStyle.container}>
          <Layout style={themedStyle.container}>
            <Text
              style={[textStyle.subtitle, themedStyle.categoryTitle]}
              category="h4"
            >
              {`Ingredientes & Alergenos`}
            </Text>
            <Layout style={themedStyle.categorySubtitleContainer}>
              <Text
                category="s1"
                appearance="hint"
                style={themedStyle.categorySubtitle}
              >
                {`${ingredientsCount} Ingredientes`}
              </Text>
              {allergensCount > 0 ? (
                <Text
                  status="warning"
                  category="s1"
                  appearance="hint"
                  style={themedStyle.categorySubtitle}
                >
                  {`${allergensCount} Alergenos`}
                </Text>
              ) : (
                <Text
                  category="s1"
                  appearance="hint"
                  style={themedStyle.categorySubtitle}
                >
                  {`${allergensCount} Alergenos`}
                </Text>
              )}
            </Layout>
          </Layout>
          <Text
            appearance="hint"
            style={[
              {
                paddingHorizontal: 16,
                paddingVertical: info.ingredients_text ? 8 : 0
              }
            ]}
          >
            {info.ingredients_text}
          </Text>
          <IngredientsTags
            style={{ paddingHorizontal: 16 }}
            data={ingredientsTags}
          />
        </Layout>

        <Layout style={themedStyle.container}>
          <Layout
            style={[
              themedStyle.container,
              { marginTop: ingredientsTags.length !== 0 ? 42 : 34 }
            ]}
          >
            <Text
              style={[textStyle.subtitle, themedStyle.categoryTitle]}
              category="h4"
            >
              Aditivos
            </Text>
          </Layout>
          <Layout style={themedStyle.categorySubtitleContainer}>
            <Text
              status="success"
              category="s1"
              style={themedStyle.categorySubtitle}
            >
              {`${additivesCount.low} Sin riesgo`}
            </Text>

            <Text
              status="warning"
              category="s1"
              style={themedStyle.categorySubtitle}
            >
              {`${additivesCount.moderate} Riesgo moderado`}
            </Text>

            <Text
              status="danger"
              category="s1"
              style={themedStyle.categorySubtitle}
            >
              {`${additivesCount.high} Riesgo alto`}
            </Text>
          </Layout>
          <AdditivesList
            data={additives}
            style={themedStyle.additivesContainer}
          />
        </Layout>

        <Layout style={{ backgroundColor: "transparent", height: 90 }} />
      </ScrollView>
    );
  }
}

export const NutrimentInfo = withStyles(NutrimentInfoComponent, theme => ({
  container: {
    backgroundColor: theme["background-basic-color-2"]
  },
  categoryTitleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: theme["background-basic-color-2"]
  },
  categoryTitle: {
    paddingHorizontal: 16,
    color: "#1b2955"
  },
  categorySubtitleContainer: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    backgroundColor: theme["background-basic-color-2"],
    paddingHorizontal: 16
  },
  categorySubtitle: {
    marginRight: 16
  },
  additivesContainer: {
    flex: 1,
    marginVertical: 8,
    marginHorizontal: 16,
    marginBottom: 24
  },
  cardContainer: {
    paddingVertical: 42,
    paddingHorizontal: 8,
    backgroundColor: theme["background-basic-color-2"]
  }
}));
