import React from "react";
import { View, StatusBar, Platform } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  withStyles
} from "react-native-ui-kitten";
import { SafeAreaView } from "./SafeAreaView";
import { textStyle } from "./style";

class HeaderNavigationComponent extends React.Component {
  onBackButtonPress = () => {
    if (this.props.onBackPress) {
      this.props.onBackPress();
    }
  };

  renderBackIcon = source => {
    const { whiteIcon } = this.props;
    return this.props.icon([source, { tintColor: whiteIcon ? "white" : null }]);
  };

  renderBackButton = () => {
    return (
      <TopNavigationAction
        icon={this.renderBackIcon}
        onPress={this.onBackButtonPress}
      />
    );
  };

  render() {
    const {
      themedStyle,
      title,
      subtitle,
      icon,
      transparent,
      alignment
    } = this.props;

    const leftControlElement = icon ? this.renderBackButton(icon) : null;

    return (
      <SafeAreaView
        style={[
          themedStyle.safeArea,
          transparent
            ? themedStyle.backgroundColorTransparent
            : themedStyle.backgroundColorWhite
        ]}
      >
        <TopNavigation
          alignment={alignment}
          title={title}
          subtitle={subtitle}
          titleStyle={textStyle.subtitle}
          subtitleStyle={textStyle.caption1}
          leftControl={leftControlElement}
          style={{ color: "#1B2955", backgroundColor: "transparent" }}
        />
      </SafeAreaView>
    );
  }
}

export const HeaderNavigation = withStyles(
  HeaderNavigationComponent,
  theme => ({
    safeArea: {
      paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
    },
    backgroundColorWhite: {
      backgroundColor: theme["background-basic-color-1"]
    },
    backgroundColorTransparent: {
      backgroundColor: "transparent"
    }
  })
);
