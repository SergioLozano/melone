import React from "react";
import { View, Image, Dimensions, Platform } from "react-native";
import { Button, withStyles } from "react-native-ui-kitten";
import { ScrollableAvoidKeyboard } from "../../common/ScrollableAvoidKeyboard";
import { SignUpForm } from "../../forms/auth/SignUpForm";
import { textStyle } from "../../common/style";
import { SocialAuthForm } from "../../forms/auth/SocialAuthForm";

const maxWidth = Dimensions.get("window").width;
const maxHeight = Dimensions.get("window").height / 6;

class SignUpComponent extends React.Component {
  state = {
    formData: undefined
  };

  onSignInButtonPress = () => {
    this.props.goBack();
  };

  onSignUpButtonPress = values => {
    this.props.onSignUpButton(values);
  };

  onFormDataChange = formData => {
    this.setState({ formData });
  };

  onGoogleButtonPress = () => {
    this.props.googleSignIn();
  };

  onFacebookButtonPress = () => {
    this.props.facebookSignIn();
  };

  onTwitterButtonPress = () => {
    this.props.twitterSignIn();
  };

  keyboardOffset = Platform.select({
    ios: 0,
    android: 40
  });

  render() {
    const { themedStyle, error, loading } = this.props;
    return (
      <ScrollableAvoidKeyboard
        style={themedStyle.container}
        //extraScrollHeight={this.keyboardOffset}
      >
        <View style={themedStyle.headerContainer}>
          <Image
            source={require("../../../assets/images/illustrations/undraw_eating_together_tjhx.png")}
            style={themedStyle.imageHeader}
            resizeMode="contain"
          />
        </View>
        <SignUpForm
          style={themedStyle.formContainer}
          onDataChange={this.onFormDataChange}
          error={error}
        />
        <Button
          style={themedStyle.signUpButton}
          textStyle={textStyle.button}
          size="giant"
          disabled={!this.state.formData || loading}
          onPress={() => this.onSignUpButtonPress(this.state.formData)}
        >
          REGISTRARSE
        </Button>
        <View style={themedStyle.socialAuthContainer}>
          <SocialAuthForm
            hint="O utiliza tu red social favorita"
            iconStyle={themedStyle.socialAuthIcon}
            onGooglePress={this.onGoogleButtonPress}
            onFacebookPress={this.onFacebookButtonPress}
            onTwitterPress={this.onTwitterButtonPress}
          />
        </View>
        <Button
          style={themedStyle.signInButton}
          textStyle={themedStyle.signUpText}
          appearance="ghost"
          activeOpacity={0.75}
          onPress={this.onSignInButtonPress}
        >
          ¿Ya tienes cuenta? Inicia Sesión
        </Button>
      </ScrollableAvoidKeyboard>
    );
  }
}

export const SignUpContainer = withStyles(SignUpComponent, theme => {
  return {
    container: {
      flex: 1,
      backgroundColor: theme["background-basic-color-2"]
    },
    headerContainer: {
      justifyContent: "center",
      alignItems: "center",
      maxHeight: maxHeight,
      marginVertical: 16,
      backgroundColor: theme["background-basic-color-2"]
    },
    imageHeader: {
      width: maxWidth,
      height: maxHeight
    },
    formContainer: {
      flex: 1,
      paddingHorizontal: 16
    },
    socialAuthContainer: {
      flex: 1,
      marginVertical: 20,
      alignItems: "center",
      width: "100%"
    },
    socialAuthIcon: {
      color: theme["color-primary-500"]
    },
    signInLabel: {
      marginVertical: 32,
      paddingBottom: 32,
      ...textStyle.subtitle
    },
    signInButton: {
      marginHorizontal: 16,
      marginVertical: 16
    },
    signUpButton: {
      marginBottom: 12,
      marginHorizontal: 16
    },
    signUpText: {
      color: theme["text-hint-color"],
      ...textStyle.subtitle
    }
  };
});
