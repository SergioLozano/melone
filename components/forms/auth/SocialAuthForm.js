import React from "react";
import { View } from "react-native";
import { withStyles, Text } from "react-native-ui-kitten";
import { Icon } from "../../UI/Icons";
import { textStyle } from "../../common/style";

class SocialAuthFormComponent extends React.Component {
  renderCaptionElement = style => {
    const { hint } = this.props;

    return <Text style={style}>{hint}</Text>;
  };

  render() {
    const {
      themedStyle,
      hintStyle,
      iconStyle,
      hint,
      ...restProps
    } = this.props;
    const { buttonContainer, ...componentStyle } = themedStyle;

    return (
      <View {...restProps}>
        {hint
          ? this.renderCaptionElement([componentStyle.hint, hintStyle])
          : null}
        <View style={buttonContainer}>
          <Icon
            raised
            name="google"
            type="font-awesome"
            iconStyle={iconStyle}
            onPress={this.props.onGooglePress}
          />
          <Icon
            raised
            name="facebook"
            type="font-awesome"
            iconStyle={iconStyle}
            onPress={this.props.onFacebookPress}
          />
          <Icon
            raised
            name="twitter"
            type="font-awesome"
            iconStyle={iconStyle}
            onPress={this.props.onTwitterPress}
          />
        </View>
      </View>
    );
  }
}

export const SocialAuthForm = withStyles(SocialAuthFormComponent, theme => ({
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    justifyContent: "space-evenly"
  },
  hint: {
    alignSelf: "center",
    marginBottom: 16,
    ...textStyle.subtitle
  }
}));
