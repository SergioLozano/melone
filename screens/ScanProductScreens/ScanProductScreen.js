import React from "react";
import {
  View,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  Platform
} from "react-native";
import { Button, Text } from "react-native-ui-kitten";
import { Camera } from "expo-camera";
import { BarCodeScanner } from "expo-barcode-scanner";
import { ContainerView, Loader } from "../../components/common";
import * as Permissions from "expo-permissions";

const { width, height } = Dimensions.get("window");
const DESIRED_RATIO = "16:9";

export default class ScanProductScreen extends React.Component {
  state = {
    hasCameraPermission: null,
    product: null,
    focusedScreen: false,
    ratio: DESIRED_RATIO
  };

  prepareRatio = async () => {
    if (Platform.OS === "android" && this.camera) {
      const ratios = await this.camera.getSupportedRatiosAsync();
      // See if the current device has your desired ratio, otherwise get the maximum supported one
      // Usually the last element of "ratios" is the maximum supported ratio
      const ratio =
        ratios.find(ratio => ratio === DESIRED_RATIO) ||
        ratios[ratios.length - 1];

      this.setState({ ratio });
    }
  };

  async componentDidMount() {
    this._getPermissionsAsync();
    const { navigation } = this.props;
    navigation.addListener("willFocus", () => {
      this.setState({ focusedScreen: true });
    });
    navigation.addListener("willBlur", () => {
      this.setState({ focusedScreen: false });
      StatusBar.setBarStyle("dark-content");
    });
  }

  _getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === "granted"
    });
    this.changeStatuBar();
  };

  handleBarCodeScanned = ({ type, data }) => {
    this.camera.pausePreview();
    this.props.navigation.navigate("ProductScreen", {
      itemRef: data,
      previous: "ScanScreen",
      resetState: this.resetState
    });
  };

  resetState = () => {
    if (this.state.focusedScreen) this.camera.resumePreview();
  };

  changeStatuBar = () => {
    this.state.hasCameraPermission
      ? StatusBar.setBarStyle("light-content")
      : StatusBar.setBarStyle("dark-content");
  };

  render() {
    const { hasCameraPermission, focusedScreen, ratio } = this.state;

    if (hasCameraPermission === false) {
      return (
        <ContainerView contentContainerStyle={styles.permissionContainer}>
          {this.changeStatuBar()}
          <View style={styles.headerContainer}>
            <Image
              source={require("../../assets/images/illustrations/undraw_camera_mg5h.png")}
              resizeMode="contain"
              style={styles.imageHeader}
            />
          </View>
          <View style={styles.informationContainer}>
            <Text
              style={{ textAlign: "center", marginBottom: 42 }}
              appearance="hint"
            >
              Para mantener un entorno donde todas las aplicaciones sean
              seguras, necesitamos pedirte permiso para utilizar la cámara. De
              esta forma podrás acceder al escáner de productos. ¡No te
              preocupes! Melone no almacena ni graba ningún tipo de imagen.
            </Text>
            <Button onPress={() => this._getPermissionsAsync()}>
              Acceder a la cámara
            </Button>
          </View>
        </ContainerView>
      );
    } else if (focusedScreen) {
      return (
        <View style={styles.container}>
          {Platform.OS === "android" ? (
            <Camera
              ratio={ratio}
              ref={ref => {
                this.camera = ref;
              }}
              onBarCodeScanned={this.handleBarCodeScanned}
              style={{ flex: 1 }}
            />
          ) : (
            <BarCodeScanner
              onBarCodeScanned={this.handleBarCodeScanned}
              style={StyleSheet.absoluteFillObject}
            />
          )}
          <View style={styles.imageContainer}>
            <Image source={require("../../assets/images/scan_border.png")} />
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.centerContainer}>
          <Loader size={80} />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  },
  centerContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F7F9FC"
  },
  permissionContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: "#F7F9FC",
    minHeight: height
  },
  headerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    maxHeight: height / 2,
    paddingTop: 80
  },
  imageHeader: {
    width: width - 32,
    height: "100%"
  },
  informationContainer: {
    position: "absolute",
    top: height / 2,
    paddingTop: 16,
    paddingHorizontal: 16
  },
  imageContainer: {
    position: "absolute",
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    bottom: height / 3
  }
});
