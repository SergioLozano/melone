import React from "react";
import { View, StyleSheet, Image, Dimensions, BackHandler } from "react-native";
import { Text, Button } from "react-native-ui-kitten";
import { connect } from "react-redux";
import { HeaderNavigation, Loader } from "../../components/common";
import { NutrimentInfo } from "../../components/product/NutrimentsInfo";
import { ArrowBackIconOutline } from "../../assets/icons";
import { addProductToHistory } from "../../redux/productHistory/productActions";

const mapState = state => ({
  product: state.product,
  loading: state.async.loading
});

const actions = {
  addProductToHistory
};

const { width, height } = Dimensions.get("window");

class ProductScreen extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener(
      "didFocus",
      payload =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
    this.state = { isLoading: true, product: undefined };
  }

  async componentDidMount() {
    const { navigation } = this.props;
    const itemRef = navigation.getParam("itemRef", null);
    if (itemRef !== null) {
      fetch(`https://world.openfoodfacts.org/api/v0/product/${itemRef}.json`)
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status !== 0) {
            this.setState({
              isLoading: false,
              product: responseJson.product
            });

            if (navigation.state.params.previous === "ScanScreen") {
              this.props.addProductToHistory(responseJson.product);
            }
          } else {
            this.setState({ isLoading: false });
          }
        });
    }

    this._willBlurSubscription = navigation.addListener(
      "willBlur",
      payload =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    const { navigation } = this.props;
    if (navigation.state.params.previous === "ScanScreen") {
      navigation.state.params.resetState();
    }
    navigation.goBack();
    return true;
  };

  render() {
    const { product, isLoading } = this.state;

    if (isLoading) {
      return <Loader />;
    }

    if (product) {
      return (
        <View>
          <HeaderNavigation
            title={product.name ? product.name : product.product_name}
            alignment="center"
            icon={ArrowBackIconOutline}
            onBackPress={this.onBackButtonPressAndroid}
          />
          <NutrimentInfo info={product} style={{ marginTop: 50 }} />
        </View>
      );
    }

    return (
      <View>
        <HeaderNavigation
          icon={ArrowBackIconOutline}
          onBackPress={this.onBackButtonPressAndroid}
        />
        <View style={styles.headerContainer}>
          <Image
            source={require("../../assets/images/illustrations/undraw_not_found_60pq.png")}
            resizeMode="contain"
            style={styles.imageHeader}
          />
        </View>
        <View style={styles.informationContainer}>
          <Text
            style={{ textAlign: "center", marginBottom: 42 }}
            appearance="hint"
          >
            ¡Vaya! Parece que este producto se esconde de nosotros. ¿Crees que
            deberiamos incluir tu producto? ¡Ayudanos a identificarlo en tres
            sencillos pasos!
          </Text>
          <Button
            onPress={() =>
              this.props.navigation.navigate("IdentifyProductScreen")
            }
          >
            Identificar Producto
          </Button>
        </View>
      </View>
    );
  }
}

export default connect(
  mapState,
  actions
)(ProductScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  },
  centerContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F7F9FC"
  },
  noProductContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "#F7F9FC"
  },
  headerContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    minHeight: height / 2
  },
  imageHeader: {
    width: width - 32,
    height: "100%"
  },
  informationContainer: {
    position: "absolute",
    top: height / 2,
    paddingTop: 16,
    paddingHorizontal: 16
  }
});
