import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Platform
} from "react-native";
import { Button, Modal, Layout, Text } from "react-native-ui-kitten";
import { HeaderNavigation } from "../../components/common";
import { IdentifyProductList } from "../../components/identifyProduct/identifyProductList";
import { ArrowBackIconOutline } from "../../assets/icons";
import { Icon } from "../../components/UI/Icons";
import { saveNewProduct } from "../../services/productActions";
import * as ImagePicker from "expo-image-picker";

width = Dimensions.get("window").width;

content = [
  {
    image: require("../../assets/images/examples/product_front.jpg"),
    text: "Toca para añadir una imagen principal del producto"
  },
  {
    image: require("../../assets/images/examples/nutrition.jpg"),
    text: "Toca para añadir una imagen de la tabla nutricional"
  },
  {
    image: require("../../assets/images/examples/ingredients.jpg"),
    text: "Toca para añadir una imagen de los ingredientes"
  }
];

class IdentifyProductScreen extends React.Component {
  state = {
    index: null,
    modalVisible: false,
    images: [null, null, null],
    imageCheker: true,
    loading: false
  };

  getPermissionAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      alert("¡Vaya! Necesitamos tu permiso para acceder a la galeria.");
      return;
    }
  };

  setImageType = (index, image) => {
    switch (index) {
      case 0:
        return { name: "front_image", uri: image };
      case 1:
        return { name: "nutrients_image", uri: image };
      case 2:
        return { name: "ingredients_image", uri: image };
      default:
        return null;
    }
  };

  onGalleryPress = async () => {
    this.modalToggle();

    if (Platform.OS === "ios") {
      this.getPermissionAsync();
    }

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1]
    });

    if (!result.cancelled) {
      const imageIndex = this.state.index;

      let imagesArr = this.state.images;
      imagesArr[imageIndex] = this.setImageType(imageIndex, result.uri);

      this.checkImages(imagesArr);
    }
  };

  openCamera = () => {
    this.modalToggle();
    this.props.navigation.navigate("CameraScreen", {
      setPicture: this.setPicture
    });
  };

  setPicture = async image => {
    const imageIndex = this.state.index;

    let imagesArr = this.state.images;
    imagesArr[imageIndex] = this.setImageType(imageIndex, image.uri);

    this.checkImages(imagesArr);
  };

  removeImage = index => {
    let imagesArr = this.state.images;
    imagesArr[index] = null;

    this.checkImages(imagesArr);
  };

  addImageButton = index => {
    this.setState({ index });
    this.modalToggle();
  };

  modalToggle = () => {
    const modalVisible = !this.state.modalVisible;

    this.setState({ modalVisible });
  };

  checkImages = images => {
    const check = images.indexOf(null);

    if (check === -1) {
      this.setState({ imageCheker: false, images });
    } else {
      this.setState({ imageCheker: true, images });
    }
  };

  saveProduct = async () => {
    this.setState({ loading: true });

    const result = await saveNewProduct(this.state.images);

    if (result.type === "success") {
      this.props.navigation.navigate("ThanksProductScreen");
    }

    if (result.type === "error") {
      Alert.alert(
        "Error",
        "Se ha producido un error en la subida de archivos, porfavor vuelva a intentarlo más tarde."
      );
    }

    this.setState({ loading: false });
  };

  renderModalElement = () => {
    return (
      <Layout level="3" style={styles.modalContainer}>
        <View style={{ flexDirection: "row" }}>
          <TouchableWithoutFeedback onPress={this.openCamera}>
            <View style={{ alignItems: "center", marginRight: 24 }}>
              <Icon
                reverse
                name="camera"
                type="entypo"
                color="#517fa4"
                size={24}
              />
              <Text appearance="hint">Cámara</Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.onGalleryPress}>
            <View style={{ alignItems: "center" }}>
              <Icon
                reverse
                name="images"
                type="entypo"
                color="#5956d6"
                size={24}
              />
              <Text appearance="hint">Galería</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </Layout>
    );
  };

  render() {
    const { images, modalVisible, imageCheker, loading } = this.state;

    return (
      <View style={styles.container}>
        <HeaderNavigation
          title="Enviar un nuevo producto"
          alignment="center"
          icon={ArrowBackIconOutline}
          onBackPress={() => {
            this.props.navigation.goBack();
          }}
        />
        <IdentifyProductList
          addImageButton={this.addImageButton}
          removeImage={this.removeImage}
          saveProduct={this.saveProduct}
          content={content}
          images={images}
          imageCheker={imageCheker}
          loading={loading}
        />
        <Modal
          allowBackdrop={true}
          backdropStyle={{ backgroundColor: "black", opacity: 0.5 }}
          onBackdropPress={this.modalToggle}
          visible={modalVisible}
        >
          {this.renderModalElement()}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  },
  modalContainer: {
    width: width - 32,
    height: 200,
    paddingVertical: 16,
    borderRadius: 12,
    alignSelf: "flex-end",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default IdentifyProductScreen;
