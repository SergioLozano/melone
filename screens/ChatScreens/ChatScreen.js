import React from "react";
import { View, StyleSheet, Image, Dimensions } from "react-native";
import { Text } from "react-native-ui-kitten";

const { width, height } = Dimensions.get("window");

export default class ChatScreen extends React.Component {
  render() {
    return (
      <View style={styles.permissionContainer}>
        <View style={styles.headerContainer}>
          <Image
            source={require("../../assets/images/illustrations/undraw_good_team_m7uu.png")}
            resizeMode="contain"
            style={styles.imageHeader}
          />
        </View>
        <View style={styles.informationContainer}>
          <Text
            style={{ textAlign: "center", marginBottom: 42 }}
            appearance="hint"
          >
            Nuestros minions se encuentran ahora mismo trabajando en este apartado de
            la aplicación, por favor no les molestes, si ya se distraen con una
            mosca... ¡no queremos ni imaginar la fiesta que montarían con todos
            vosotros!
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F7F9FC"
  },
  permissionContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: "#F7F9FC"
  },
  headerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    maxHeight: height / 2,
    paddingTop: 80
  },
  imageHeader: {
    width: width - 32,
    height: "100%"
  },
  informationContainer: {
    position: "absolute",
    top: height / 2,
    paddingTop: 16,
    paddingHorizontal: 16
  }
});
