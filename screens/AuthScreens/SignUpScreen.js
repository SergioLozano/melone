import React, { Component } from "react";
import { View, StyleSheet, Alert } from "react-native";
import { HeaderNavigation } from "../../components/common/HeaderNavigation";
import { SignUpContainer } from "../../components/containers/auth/SignUpContainer";
import { Loader } from "../../components/common";
import { ArrowBackIconOutline } from "../../assets/icons";
import {
  initGoogleSignInAsync,
  signInWithGoogle,
  signInWithFacebook,
  signInWithTwitter,
  signUpWithEmail
} from "../../services/authActions";

class SignUpScreen extends Component {
  state = {
    error: undefined,
    laoding: false,
    loginLoader: false
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  componentDidMount() {
    initGoogleSignInAsync();
  }

  googleSignIn = () => {
    signInWithGoogle()
      .then(result => {
        this.setState({ loginLoader: true });
        if (result.type === "success") {
          this.props.navigation.navigate("Dashboard");
        }
      })
      .catch(err => {
        this.setState({ loginLoader: false });
        if (err.type !== "cancel") {
          Alert.alert(
            "Error",
            "Se ha producido un error en el inicio de sesión con Google vuelva a intentarlo más tarde."
          );
        }
      });
  };

  facebookSignIn = () => {
    signInWithFacebook()
      .then(result => {
        this.setState({ loginLoader: true });
        if (result.type === "success") {
          this.props.navigation.navigate("Dashboard");
        }
      })
      .catch(err => {
        this.setState({ loginLoader: false });
        if (err.type !== "cancel") {
          Alert.alert(
            "Error",
            "Se ha producido un error en el inicio de sesión con Facebook vuelva a intentarlo más tarde."
          );
        }
      });
  };

  twitterSignIn = () => {
    this.setState({ loginLoader: true });
    try {
      signInWithTwitter();
    } catch {
      this.setState({ loginLoader: false });
      Alert.alert(
        "Error",
        "Se ha producido un error en el inicio de sesión con Twitter vuelva a intentarlo más tarde."
      );
    }
  };

  onSignUpButton = values => {
    this.setState({ loading: true, error: undefined });
    signUpWithEmail(values).catch(error =>
      this.setState({ error, loading: false })
    );
  };

  render() {
    const { error, loading, loginLoader } = this.state;
    if (loginLoader) {
      <Loader />;
    } else {
      return (
        <View style={styles.container}>
          <HeaderNavigation
            transparent
            icon={ArrowBackIconOutline}
            onBackPress={() => {
              this.props.navigation.goBack();
            }}
          />
          <SignUpContainer
            goBack={() => this.goBack()}
            onSignUpButton={this.onSignUpButton}
            googleSignIn={() => this.googleSignIn()}
            facebookSignIn={() => this.facebookSignIn()}
            twitterSignIn={() => this.twitterSignIn()}
            error={error}
            loading={loading}
          />
        </View>
      );
    }
  }
}

export default SignUpScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  }
});
