import React, { Component } from "react";
import { View, StyleSheet, Alert } from "react-native";
import {
  initGoogleSignInAsync,
  signInWithGoogle,
  signInWithFacebook,
  signInWithTwitter,
  signInWithEmail
} from "../../services/authActions";
import { SignInContainer } from "../../components/containers/auth/SignInContainer";
import { Loader } from "../../components/common";

class SignInScreen extends Component {
  state = {
    error: undefined,
    laoding: false,
    loginLoader: false
  };

  componentDidMount() {
    initGoogleSignInAsync();
  }

  googleSignIn = () => {
    signInWithGoogle()
      .then(result => {
        this.setState({ loginLoader: true });
        if (result.type === "success") {
          this.props.navigation.navigate("Dashboard");
        }
      })
      .catch(err => {
        this.setState({ loginLoader: false });
        if (err.type !== "cancel") {
          Alert.alert(
            "Error",
            "Se ha producido un error en el inicio de sesión con Google vuelva a intentarlo más tarde."
          );
        }
      });
  };

  facebookSignIn = () => {
    signInWithFacebook()
      .then(result => {
        this.setState({ loginLoader: true });
        if (result.type === "success") {
          this.props.navigation.navigate("Dashboard");
        }
      })
      .catch(err => {
        this.setState({ loginLoader: false });
        if (err.type !== "cancel") {
          Alert.alert(
            "Error",
            "Se ha producido un error en el inicio de sesión con Facebook vuelva a intentarlo más tarde."
          );
        }
      });
  };

  twitterSignIn = () => {
    this.setState({ loginLoader: true });
    try {
      signInWithTwitter();
    } catch {
      this.setState({ loginLoader: false });
      Alert.alert(
        "Error",
        "Se ha producido un error en el inicio de sesión con Twitter vuelva a intentarlo más tarde."
      );
    }
  };

  onSignInButton = values => {
    this.setState({ loading: true, error: undefined });
    signInWithEmail(values).catch(error =>
      this.setState({ error, loading: false })
    );
  };

  onSignUpButton = () => {
    this.props.navigation.navigate("SignUp");
  };

  onForgotPasswordButton = () => {
    this.props.navigation.navigate("ForgotPassword");
  };

  render() {
    const { error, loading, loginLoader } = this.state;
    if (loginLoader) {
      <Loader />;
    } else {
      return (
        <View style={styles.container}>
          <SignInContainer
            googleSignIn={() => this.googleSignIn()}
            facebookSignIn={() => this.facebookSignIn()}
            twitterSignIn={() => this.twitterSignIn()}
            onSignInButton={this.onSignInButton}
            onSignUpButton={() => this.onSignUpButton()}
            onForgotPasswordButton={() => this.onForgotPasswordButton()}
            error={error}
            loading={loading}
          />
        </View>
      );
    }
  }
}
export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  }
});
