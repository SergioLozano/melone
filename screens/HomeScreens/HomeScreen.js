import React from "react";
import { View, StyleSheet, Platform, StatusBar } from "react-native";
import ArticleListContainer from "../../components/containers/articles/ArticleListContainer";

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ArticleListContainer />
      </View>
    );
  }
}
export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC",
    paddingTop: Platform.select({
      ios: StatusBar.currentHeight,
      android: StatusBar.currentHeight
    })
  }
});
