import React from "react";
import { View, Platform, StyleSheet } from "react-native";
import {Button} from "react-native-ui-kitten"
import { ProfileSettings } from "../../components/containers/profile/ProfileContainer";
import { signOut } from "../../services/authActions";
import { updateDisplayName, savePhotoURL } from "../../redux/user/userActions";
import { connect } from "react-redux";
import * as ImagePicker from "expo-image-picker";
import * as ImageManipulator from "expo-image-manipulator";
import * as Permissions from "expo-permissions";

const mapState = state => ({
  user: state.user
});

const actions = {
  updateDisplayName,
  savePhotoURL
};

class ProfileScreen extends React.Component {
  state = {
    profile: this.props.user,
    loading: false
  };

  getPermissionAsync = async () => {
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("¡Vaya! Necesitamos tu permiso para acceder a la galeria.");
        return;
      }
    }
  };

  onPhotoButtonPress = async () => {
    const { savePhotoURL, user } = this.props;
    this.getPermissionAsync();
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3]
    });

    if (!result.cancelled) {
      const resizeResult = await ImageManipulator.manipulateAsync(result.uri, [
        { compress: 1, resize: { width: 180 } }
      ]);

      this.setState(prevState => ({
        profile: {
          ...prevState.profile,
          photoURL: result.uri
        }
      }));

      savePhotoURL(user, resizeResult.uri);
    }
  };

  onCloseSessionPress = () => {
    signOut(this.state.profile.provider);
  };

  onEditUserAction = displayName => {
    this.props.updateDisplayName(this.state.profile, displayName);
  };

  render() {
    if (this.state.loading) {
      return <></>;
    }
    return (
      <View style={styles.container}>
        <ProfileSettings
          profile={this.state.profile}
          onPhotoButtonPress={this.onPhotoButtonPress}
          onCloseSessionPress={this.onCloseSessionPress}
          onEditUserAction={this.onEditUserAction}
        />
        {/* <Button onPress={() => this.props.navigation.navigate("TestScreen")}>Test Screen</Button> */}
      </View>
    );
  }
}

export default connect(
  mapState,
  actions
)(ProfileScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  }
});
