import React, { Component } from "react";
import { connect } from "react-redux";
import { getUser } from "../../redux/user/userActions";
import { Loader } from "../../components/common";
import firebase from "firebase";

const mapState = state => ({
  user: state.user
});

const actions = {
  getUser
};

class LoadingScreen extends Component {
  componentDidMount() {
    this.checkIfLoggedIn();
  }

  checkIfLoggedIn = () => {
    firebase.auth().onAuthStateChanged(
      function(user) {
        //console.log('AUTH STATE CHANGED CALLED ')
        if (user && !user.isAnonymous) {
          this.props.getUser(user.uid)
          this.props.navigation.navigate("Dashboard");
        } else {
          firebase.auth().signInAnonymously();
          this.props.navigation.navigate("Auth");
        }
      }.bind(this)
    );
  };

  render() {
    return <Loader />;
  }
}
export default connect(
  mapState,
  actions
)(LoadingScreen);
