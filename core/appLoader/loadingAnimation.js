import React from "react";
import { Animated, Easing, StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default class LoadingAnimationComponent extends React.Component {
  constructor() {
    super(...arguments);
    this.state = {
      animationValue: new Animated.Value(0),
      animationCompleted: false
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.isLoaded && nextProps.isLoaded !== this.props.isLoaded) {
      this.triggerAnimation();
    }
  }

  triggerAnimation() {
    Animated.timing(this.state.animationValue, {
      toValue: 1,
      duration: 700,
      useNativeDriver: true,
      easing: Easing.in(Easing.exp)
    }).start(() => this.onAnimationCompleted());
  }

  onAnimationCompleted() {
    this.setState({ animationCompleted: true });
  }

  renderAnimatedComponent() {
    const opacity = this.state.animationValue.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0]
    });
    const transform = [
      {
        scale: this.state.animationValue.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 1.5]
        })
      }
    ];

    return (
      <Animated.View style={[styles.container, { opacity }]}>
        <Animated.Image
          source={require("../../assets/images/splash.png")}
          style={[styles.image, { transform }]}
        />
      </Animated.View>
    );
  }

  render() {
    const { animationCompleted } = this.state;
    return animationCompleted ? null : this.renderAnimatedComponent();
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2b2c6b"
  },
  image: {
    width: width - 24,
    height: height,
    //...StyleSheet.absoluteFillObject,
    resizeMode: "contain"
  }
});
