import React from "react";
import { SplashScreen, AppLoading } from "expo";
import { Asset } from "expo-asset";
import * as Font from 'expo-font'

import LoadingAnimationComponent from "./loadingAnimation";

export class ApplicationLoader extends React.Component {
  constructor(props) {
    super(props);
    SplashScreen.preventAutoHide();
  }

  state = {
    loaded: false
  };

  onLoadSuccess = () => {
    this.setState({ loaded: true });
    SplashScreen.hide();
  };

  onLoadError = error => {
    console.warn(error);
  };

  // async componentDidMount() {
  //   const { fonts, images } = assets;
  //   await Font.loadAsync(fonts)
  // }

  loadResources = () => {
    return this.loadResourcesAsync(this.props.assets);
  };

  loadFonts = fonts => {
    return Font.loadAsync(fonts);
  };

  loadImages = images => {
    const tasks = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    return Promise.all(tasks);
  };

  loadResourcesAsync = assets => {
    const { fonts, images } = assets;
    return Promise.all([this.loadFonts(fonts), this.loadImages(images)]);
  };

  renderLoading = () => (
    <AppLoading
      startAsync={this.loadResources}
      onFinish={this.onLoadSuccess}
      onError={this.onLoadError}
      autoHideSplash={false}
    />
  );

  render() {
    return (
      <React.Fragment>
        {this.state.loaded ? this.props.children : this.renderLoading()}
        <LoadingAnimationComponent isLoaded={this.state.loaded} />
      </React.Fragment>
    );
  }
}
