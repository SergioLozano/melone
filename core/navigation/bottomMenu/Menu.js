import React from "react";
import {
  ThemeProvider,
  withStyles,
  BottomNavigation,
  BottomNavigationTab
} from "react-native-ui-kitten";
import { SafeAreaView } from "../../../components/common";
import { themes } from "../../themes";
import {
  HomeIconOutline,
  MessageCircleIconOutline,
  CubeIconOutline,
  ClockIconOutline,
  PersonIconOutline
} from "../../../assets/icons";

class MenuComponent extends React.Component {
  onTabSelect = index => {
    this.props.onTabSelect(index);
  };

  render() {
    const { selectedIndex, themedStyle } = this.props;

    return (
      <SafeAreaView style={themedStyle.safeAreaContainer}>
        <ThemeProvider theme={{ ...this.props.theme, ...themes["App Theme"] }}>
          <BottomNavigation
            appearance="noIndicator"
            selectedIndex={selectedIndex}
            onSelect={this.onTabSelect}
          >
            <BottomNavigationTab title="Inicio" icon={HomeIconOutline} />
            {/* <BottomNavigationTab title="Chat" icon={MessageCircleIconOutline} /> */}
            <BottomNavigationTab title="Escanear" icon={CubeIconOutline} />
            <BottomNavigationTab title="Historial" icon={ClockIconOutline} />
            <BottomNavigationTab title="Perfil" icon={PersonIconOutline} />
          </BottomNavigation>
        </ThemeProvider>
      </SafeAreaView>
    );
  }
}

export const Menu = withStyles(MenuComponent, theme => ({
  safeAreaContainer: {
    backgroundColor: theme["background-basic-color-1"]
  }
}));
