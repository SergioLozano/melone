import React from 'react';
import {
  createAppContainer,
  createSwitchNavigator,
  createStackNavigator
} from "react-navigation";
import { connect } from 'react-redux';

//LoadingStack
import LoadingScreen from "../../screens/LoadingScreens/LoadingScreen";

//AuthStack
import SignInScreen from "../../screens/AuthScreens/SignInScreen";
import SignUpScreen from "../../screens/AuthScreens/SignUpScreen";
import ForgotPasswordScreen from "../../screens/AuthScreens/ForgotPasswordScreen";

//HomeStack
import { TabNavigatorScreen } from "./MainTabNavigator";

const AuthStack = createStackNavigator(
  {
    SignIn: { screen: SignInScreen, params: { statusbar: "dark-content" } },
    SignUp: { screen: SignUpScreen, params: { statusbar: "dark-content" } },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      params: { statusbar: "dark-content" }
    }
  },
  {
    headerMode: "screen",
    defaultNavigationOptions: {
      header: null
    }
  }
);

const HomeStack = createStackNavigator(
  {
    MainScreen: {
      screen: TabNavigatorScreen,
      params: { statusbar: "dark-content" }
    }
  },
  {
    headerMode: "screen",
    defaultNavigationOptions: {
      header: null
    }
  }
);

export const StackNavigation = createSwitchNavigator({
  Loading: LoadingScreen,
  Auth: AuthStack,
  Dashboard: HomeStack
});

const mapStateToProps = state => ({
  nav: state.nav,
});

const AppNavigator = createAppContainer(StackNavigation);

export default connect(mapStateToProps)(AppNavigator);