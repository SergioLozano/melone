import { default as appTheme } from "./appTheme.json";
import { light as lightTheme } from "@eva-design/eva";
import { dark as darkTheme } from "@eva-design/eva";

const light = {
  ...lightTheme,
  ...appTheme
};

const dark = {
  ...darkTheme,
  ...appTheme
};

export const themes = {
  "Eva Light": light,
  "Eva Dark": dark,
  "App Theme": appTheme
};
export { ThemeContext } from "./themeContext";
export { ThemeStore } from "./themeStore";
export { ThemeService } from "./themeService";
