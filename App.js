import "./utils/fixtimerbug";

import React from "react";
import { StyleSheet, StatusBar } from "react-native";
import { SafeAreaView } from "./components/common";
import { mapping } from "@eva-design/eva";
import { ApplicationProvider } from "react-native-ui-kitten";
import { useScreens } from "react-native-screens";
import { Provider } from "react-redux";
import { store } from "./redux/app-redux";
import { ApplicationLoader } from "./core/appLoader/applicationLoader";
import { DynamicStatusBar } from "./components/common/DynamicStatusBar";
import { ThemeContext, themes, ThemeStore } from "./core/themes";
import { Ionicons } from "@expo/vector-icons";

import AppNavigator from "./core/navigation/AppNavigator";

import * as firebase from "firebase";
import { firebaseConfig } from "./firebase";
firebase.initializeApp(firebaseConfig);

useScreens();

const fonts = {
  "opensans-semibold": require("./assets/fonts/opensans-semibold.ttf"),
  "opensans-bold": require("./assets/fonts/opensans-bold.ttf"),
  "opensans-extrabold": require("./assets/fonts/opensans-extra-bold.ttf"),
  "opensans-light": require("./assets/fonts/opensans-light.ttf"),
  "opensans-regular": require("./assets/fonts/opensans-regular.ttf"),
  "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf"),
  ...Ionicons.font
};

const images = [
  require("./assets/images/illustrations/undraw_camera_mg5h.png"),
  require("./assets/images/illustrations/undraw_different_love_a3rg.png"),
  require("./assets/images/illustrations/undraw_eating_together_tjhx.png"),
  require("./assets/images/illustrations/undraw_good_team_m7uu.png"),
  require("./assets/images/illustrations/undraw_not_found_60pq.png"),
  require("./assets/images/illustrations/undraw_time_management_30iu.png"),
  require("./assets/images/illustrations/undraw_super_thank_you_obwk.png"),
  require("./assets/icons/flat/allergen.png"),
  require("./assets/icons/flat/butter.png"),
  require("./assets/icons/flat/calories.png"),
  require("./assets/icons/flat/fabric.png"),
  require("./assets/icons/flat/molecule.png"),
  require("./assets/icons/flat/salt.png"),
  require("./assets/icons/flat/sugar.png")
];

const assets = {
  images: images,
  fonts: fonts
};

function getActiveRoute(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRoute(route);
  }
  return route;
}

export default class App extends React.Component {
  state = {
    theme: "Eva Light"
  };

  onSwitchTheme = theme => {
    ThemeStore.setTheme(theme).then(() => {
      this.setState({ theme });
    });
  };
  render() {
    const contextValue = {
      currentTheme: this.state.theme,
      toggleTheme: this.onSwitchTheme
    };
    return (
      <ApplicationLoader assets={assets}>
        <ThemeContext.Provider value={contextValue}>
          <ApplicationProvider
            mapping={mapping}
            theme={themes[this.state.theme]}
          >
            <DynamicStatusBar currentTheme={this.state.theme} />
            <SafeAreaView style={styles.container}>
              <Provider store={store}>
                <AppNavigator
                  onNavigationStateChange={(prevState, currentState) => {
                    const currentScreen = getActiveRoute(currentState);
                    const prevScreen = getActiveRoute(prevState);
                    if (prevScreen.routeName !== currentScreen.routeName) {
                      const statusTheme = currentScreen.params.statusbar;
                      StatusBar.setBarStyle(statusTheme);
                    }
                  }}
                />
              </Provider>
            </SafeAreaView>
          </ApplicationProvider>
        </ThemeContext.Provider>
      </ApplicationLoader>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F9FC"
  }
});
