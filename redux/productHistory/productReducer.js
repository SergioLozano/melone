import { createReducer } from "../util/reducerUtil";
import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  FETCH_PRODUCTS
} from "./productConstants";

const initialState = [];

export const addProduct = (state, payload) => {
  state = state.filter(product => {
    return product.id !== payload.product.id;
  });

  const newProductsArray = [payload.product].concat(state);

  return newProductsArray;
};

export const fetchProducts = (state, payload) => {
  return payload.products;
};

export const removeProduct = (state, payload) => {
  return [...state.filter(product => product.id !== payload.product.id)];
};

export default createReducer(initialState, {
  [ADD_PRODUCT]: addProduct,
  [REMOVE_PRODUCT]: removeProduct,
  [FETCH_PRODUCTS]: fetchProducts
});
