import { createStore, applyMiddleware, combineReducers } from "redux";
import thunkMiddleware from "redux-thunk";
import testReducer from "./test/testReducer";
import asyncReducer from "./async/asyncReducer";
import articlesReducer from "./articles/articlesReducer";
import userReducer from "./user/userReducer";
import productReducer from "./productHistory/productReducer";

const rootReducer = combineReducers({
  test: testReducer,
  async: asyncReducer,
  articles: articlesReducer,
  user: userReducer,
  products: productReducer
});

//
// Store...
//

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));
export { store };
