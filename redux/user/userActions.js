import { FETCH_USER, UPDATE_PROFILE } from "./userConstants";
import { AsyncStorage } from "react-native";
import * as firebase from "firebase";
import "@firebase/firestore";

export const getUser = uid => async (dispatch, getState) => {
  const firestore = firebase.firestore();
  const usersRef = firestore.collection("Users");

  const localUserString = await AsyncStorage.getItem("user");
  const user = JSON.parse(localUserString);

  if (user !== null) {
    dispatch({ type: FETCH_USER, payload: { user } });
  } else {
    try {
      let query = usersRef.doc(uid);
      let querySnap = await query.get();

      let userData = querySnap.data();

      if (userData) {
        userData.uid = uid;
        await AsyncStorage.setItem("user", JSON.stringify(userData));
        dispatch({ type: FETCH_USER, payload: { user: userData } });
      }
    } catch (err) {
      console.log(err);
    }
  }
};

export const savePhotoURL = (userData, file) => async (
  dispatch,
  getState
) => {
  const user = firebase.auth().currentUser;
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users").doc(user.uid);
  const path = `users/${user.uid}/profile_images/`;
  const ref = firebase.storage().ref(path);
  const imageName = "profile";
  const ext = file.split(".").pop();
  const response = await fetch(file);
  const blob = await response.blob();

  var uploadTask = ref.child(`${imageName}.${ext}`).put(blob);

  uploadTask.on(
    "state_changed",
    function(snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log("Upload is " + progress + "% done");
    },
    function(error) {
      console.log(error);
    },
    function() {
      uploadTask.snapshot.ref.getDownloadURL().then(async function(downloadURL) {
        userRef.update({ photoURL: downloadURL });
        user.updateProfile({ photoURL: downloadURL });
        userData.photoURL = downloadURL;
        await AsyncStorage.setItem("user", JSON.stringify(userData));
        dispatch({ type: UPDATE_PROFILE, payload: { user: userData } });
      });
    }
  );
}

export const updateDisplayName = (userData, displayName) => async (
  dispatch,
  getState
) => {
  const user = firebase.auth().currentUser;
  const firestore = firebase.firestore();
  const userRef = firestore.collection("Users").doc(user.uid);

  await userRef.update({ displayName });

  userData.displayName = displayName;

  if (displayName) {
    await AsyncStorage.setItem("user", JSON.stringify(userData));
    dispatch({ type: UPDATE_PROFILE, payload: { user: userData } });
  }
};
